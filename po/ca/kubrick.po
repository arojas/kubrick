# Translation of kubrick.po to Catalan
# Copyright (C) 2008-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Albert Astals Cid <aacid@kde.org>, 2008.
# Manuel Tortosa <manutortosa@gmail.com>, 2009.
# Josep M. Ferrer <txemaq@gmail.com>, 2010, 2012, 2014, 2015, 2017, 2019, 2020, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kubrick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-13 01:42+0000\n"
"PO-Revision-Date: 2021-10-22 19:58+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Albert Astals Cid"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "aacid@kde.org"

#: game.cpp:82
#, kde-format
msgid "Front View"
msgstr "Vista frontal"

#: game.cpp:83
#, kde-format
msgid "Back View"
msgstr "Vista posterior"

#: game.cpp:90
#, kde-format
msgid "DEMO - Click anywhere to begin playing"
msgstr "DEMOSTRACIÓ - Cliqueu a qualsevol lloc perquè comenci"

#: game.cpp:145
#, kde-format
msgid ""
"Sorry, the cube cannot be shuffled at the moment.  The number of shuffling "
"moves is set to zero.  Please select your number of shuffling moves in the "
"options dialog at menu item Game->Choose Puzzle Type->Make Your Own..."
msgstr ""
"Ho sentim, en aquests moments no es pot barrejar el cub. El nombre de "
"moviments per a barrejar està posat a zero. Si us plau, seleccioneu el "
"nombre de moviments de barreja al diàleg d'opcions del menú «Joc -> Tria el "
"tipus de trencaclosques -> Personalitza...»"

#: game.cpp:149
#, kde-format
msgid "New Puzzle"
msgstr "Trencaclosques nou"

#: game.cpp:165
#, kde-format
msgid "Load Puzzle"
msgstr "Obre un trencaclosques"

#: game.cpp:166 game.cpp:1333
#, kde-format
msgid "Kubrick Game Files (*.kbk)"
msgstr "Fitxers del joc Kubrick (*.kbk)"

#: game.cpp:175
#, kde-format
msgid "The file '%1' is not a valid Kubrick game-file."
msgstr "El fitxer «%1» no és un fitxer vàlid del joc Kubrick."

#: game.cpp:235
#, kde-format
msgid "Undo"
msgstr "Desfés"

#: game.cpp:241
#, kde-format
msgid "Redo"
msgstr "Refés"

#: game.cpp:251
#, kde-format
msgid "This cube has not been shuffled, so there is nothing to solve."
msgstr "Aquest cub no ha estat barrejat, per tant no hi ha res a resoldre."

#: game.cpp:253
#, kde-format
msgid "Solve the Cube"
msgstr "Resol el cub"

#: game.cpp:350
#, kde-format
msgid "Restart Puzzle (Undo All)"
msgstr "Reinicia el trencaclosques (desfà tots els moviments)"

#: game.cpp:356 kubrick.cpp:292
#, kde-format
msgid "Redo All"
msgstr "Refés Tot"

#: game.cpp:417
#, kde-format
msgid ""
"Sorry, could not find a valid Kubrick demo file called %1.  It should have "
"been installed in the 'apps/kubrick' sub-directory."
msgstr ""
"Disculpeu, no s'ha pogut trobar un fitxer vàlid de demostració del Kubrick "
"anomenat %1. Hauria d'estar instal·lat al subdirectori «apps/kubrick»."

#: game.cpp:420
#, kde-format
msgid "File Not Found"
msgstr "No s'ha trobat el fitxer"

#: game.cpp:1044
#, kde-format
msgid "Only one of your dimensions can be one cubie wide."
msgstr "Només una de les dimensions del cub pot tenir una amplada d'u."

#: game.cpp:1045
#, kde-format
msgid "Cube Options"
msgstr "Opcions del cub"

#: game.cpp:1332
#, kde-format
msgid "Save Puzzle"
msgstr "Desa el trencaclosques"

#: game.cpp:1545
#, kde-format
msgid "You have no moves to undo."
msgstr "No hi ha moviments a desfer."

#: game.cpp:1575
#, kde-format
msgid ""
"There are no moves to redo.\n"
"\n"
"That could be because you have not undone any or you have redone them all or "
"because all previously undone moves are automatically deleted whenever you "
"make a new move using the keyboard or mouse."
msgstr ""
"No hi ha cap moviment a refer.\n"
"\n"
"Això pot ser perquè no n'heu desfet cap o perquè ja els heu refet tots o "
"perquè tots els moviments desfets prèviament se suprimeixen automàticament "
"sempre que feu un moviment nou usant el teclat o el ratolí."

#: game.cpp:1866
#, kde-format
msgid ""
"The cube has animated moves in progress or the demo is running.\n"
"\n"
"Please wait or click on the cube to stop the demo."
msgstr ""
"El cub s'està movent o s'està executant una demostració.\n"
"\n"
"Si us plau, espereu o cliqueu al cub per a aturar la demostració."

#: game.cpp:1869
#, kde-format
msgid "Sorry, too busy."
msgstr "Ho sentim, massa enfeinat."

#: gamedialog.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Rubik's Cube Options"
msgstr "Opcions del cub de Rubik"

#: gamedialog.cpp:48
#, kde-format
msgid "Cube dimensions:"
msgstr "Dimensions del cub:"

#: gamedialog.cpp:61
#, kde-format
msgid "Moves per shuffle (difficulty):"
msgstr "Moviments per a barrejar (dificultat):"

#: gamedialog.cpp:68
#, kde-format
msgid "Cube dimensions: %1x%2x%3"
msgstr "Dimensions del cub: %1x%2x%3"

#: gamedialog.cpp:74
#, kde-format
msgid "Moves per shuffle (difficulty): %1"
msgstr "Moviments per a barrejar (dificultat): %1"

#: gamedialog.cpp:81
#, kde-format
msgid ""
"<i>Please use <nobr>'Choose Puzzle Type->Make Your Own...'</nobr> to set the "
"above options.</i>"
msgstr ""
"<i>Si us plau, useu <nobr>«Tria el tipus de trencaclosques -> "
"Personalitza...»</nobr> per a definir les opcions de sobre.</i>"

#: gamedialog.cpp:99
#, kde-format
msgid "Watch shuffling in progress?"
msgstr "Voleu veure el procés de barreja?"

#: gamedialog.cpp:102
#, kde-format
msgid "Watch your moves in progress?"
msgstr "Voleu veure els vostres moviments?"

#: gamedialog.cpp:108
#, kde-format
msgid "Speed of moves:"
msgstr "Velocitat dels moviments:"

#: gamedialog.cpp:118
#, no-c-format, kde-format
msgid "% of bevel on edges of cubies:"
msgstr "% de bisell a les vores dels cubs petits:"

#: gamedialog.cpp:175
#, kde-format
msgid ""
"You can choose any size of cube (or brick) up to 6x6x6, but only one side "
"can have dimension 1 (otherwise the puzzle becomes trivial).  The easiest "
"puzzle is 2x2x1 and 3x3x1 is a good warmup for the original Rubik's Cube, "
"which is 3x3x3.  Simple puzzles have 2 to 5 shuffling moves, a difficult "
"3x3x3 puzzle has 10 to 20 --- or you can choose zero shuffling then shuffle "
"the cube yourself, maybe for a friend to solve.\n"
"The other options determine whether you can watch the shuffling and/or your "
"own moves and how fast the animation goes.  The bevel option affects the "
"appearance of the small cubes.  Try setting it to 30 and you'll see what we "
"mean."
msgstr ""
"Podeu triar qualsevol mida del cub (o bric) fins a 6x6x6, però només una "
"cara pot tenir una mida d'1 (si no el trencaclosques seria trivial). El "
"trencaclosques més fàcil és el de 2x2x1 i el de 3x3x1 és un bon escalfament "
"per al cub de Rubik original, el qual és de 3x3x3. Els trencaclosques fàcils "
"tenen de 2 a 5 moviments de barreja, un trencaclosques difícil de 3x3x3 en "
"té de 10 a 20 --- o podeu escollir zero barreges i després barrejar el cub "
"vosaltres mateixos, potser perquè ho resolgui un amic.\n"
"Les altres opcions determinen si podeu veure com es barreja i/o els vostres "
"propis moviments i com és de ràpida l'animació. L'opció del bisell efecte "
"l'aparença dels cubs petits. Proveu de posar-ho a 30 i veureu què volem dir."

#: gamedialog.cpp:187
#, kde-format
msgid "HELP: Rubik's Cube Options"
msgstr "AJUDA: Opcions del cub de Rubik"

#: kubrick.cpp:68
#, kde-format
msgid "Welcome to Kubrick"
msgstr "Us donem la benvinguda al Kubrick"

#: kubrick.cpp:89
msgid "2x2x1 mat, 1 move"
msgstr "Matriu de 2x2x1, 1 moviment"

#: kubrick.cpp:90
msgid "2x2x1 mat, 2 moves"
msgstr "Matriu de 2x2x1, 2 moviments"

#: kubrick.cpp:91
msgid "2x2x1 mat, 3 moves"
msgstr "Matriu de 2x2x1, 3 moviments"

#: kubrick.cpp:92
msgid "2x2x2 cube, 2 moves"
msgstr "Cub de 2x2x2, 2 moviments"

#: kubrick.cpp:93
msgid "2x2x2 cube, 3 moves"
msgstr "Cub de 2x2x2, 3 moviments"

#: kubrick.cpp:94
msgid "2x2x2 cube, 4 moves"
msgstr "Cub de 2x2x2, 4 moviments"

#: kubrick.cpp:95
msgid "3x3x1 mat, 4 moves"
msgstr "Matriu de 3x3x1, 4 moviments"

#: kubrick.cpp:96 kubrick.cpp:103
msgid "3x3x3 cube, 3 moves"
msgstr "Cub de 3x3x3, 3 moviments"

#: kubrick.cpp:97 kubrick.cpp:104
msgid "3x3x3 cube, 4 moves"
msgstr "Cub de 3x3x3, 4 moviments"

#: kubrick.cpp:105
msgid "4x4x4 cube, 4 moves"
msgstr "Cub de 4x4x4, 4 moviments"

#: kubrick.cpp:106
msgid "5x5x5 cube, 4 moves"
msgstr "Cub de 5x5x5, 4 moviments"

#: kubrick.cpp:107
msgid "6x3x2 brick, 4 moves"
msgstr "Bric de 6x3x2, 4 moviments"

#: kubrick.cpp:113
msgid "3x3x3 cube, 7 moves"
msgstr "Cub de 3x3x3, 7 moviments"

#: kubrick.cpp:114
msgid "4x4x4 cube, 5 moves"
msgstr "Cub de 4x4x4, 5 moviments"

#: kubrick.cpp:115
msgid "5x5x5 cube, 6 moves"
msgstr "Cub de 5x5x5, 6 moviments"

#: kubrick.cpp:116
msgid "6x6x6 cube, 6 moves"
msgstr "Cub de 6x6x6, 6 moviments"

#: kubrick.cpp:117
msgid "6x4x1 mat, 9 moves"
msgstr "Matriu de 6x4x1, 9 moviments"

#: kubrick.cpp:118
msgid "6x3x2 brick, 6 moves"
msgstr "Bric de 6x3x2, 6 moviments"

#: kubrick.cpp:124
msgid "3x3x3 cube, 12 moves"
msgstr "Cub de 3x3x3, 12 moviments"

#: kubrick.cpp:125
msgid "3x3x3 cube, 15 moves"
msgstr "Cub de 3x3x3, 15 moviments"

#: kubrick.cpp:126
msgid "3x3x3 cube, 20 moves"
msgstr "Cub de 3x3x3, 20 moviments"

#: kubrick.cpp:127
msgid "4x4x4 cube, 12 moves"
msgstr "Cub de 4x4x4, 12 moviments"

#: kubrick.cpp:128
msgid "5x5x5 cube, 15 moves"
msgstr "Cub de 5x5x5, 15 moviments"

#: kubrick.cpp:129
msgid "6x6x6 cube, 25 moves"
msgstr "Cub de 6x6x6, 25 moviments"

#: kubrick.cpp:135
msgid ""
"Rubik's Cube can be moved into many interesting patterns.  Here are a few "
"from David Singmaster's classic book 'Notes on Rubik's Magic Cube, Fifth "
"Edition', pages 47-49, published in 1981.  After a pattern has formed, you "
"can use the Solve action (default key S) to undo and redo it as often as you "
"like."
msgstr ""
"El cub de Rubik es pot moure formant diferents patrons interessants. Aquí "
"n'hi ha uns quants del clàssic llibre de David Singmaster «Notes on Rubik's "
"Magic Cube, Fifth Edition», pàgines 47-49, publicat el 1981. Quan el patró "
"està format, podeu usar l'opció Resoldre (tecla per defecte S) per a desfer-"
"lo i refer-lo tantes vegades com vulgueu."

#: kubrick.cpp:144 kubrick.cpp:179
msgid "Info"
msgstr "Informació"

#: kubrick.cpp:145
msgid "3x3x3, 6 X"
msgstr "3x3x3, 6 X"

#: kubrick.cpp:146
msgid "3x3x3, 2 X"
msgstr "3x3x3, 2 X"

#: kubrick.cpp:147
msgid "3x3x3, 6 Spot"
msgstr "3x3x3, 6 punts"

#: kubrick.cpp:148
msgid "3x3x3, 4 Spot"
msgstr "3x3x3, 4 punts"

#: kubrick.cpp:149
msgid "3x3x3, 4 Plus"
msgstr "3x3x3, 4 més"

#: kubrick.cpp:150
msgid "3x3x3, 4 Bar"
msgstr "3x3x3, 4 barres"

#: kubrick.cpp:151
msgid "3x3x3, 6 U"
msgstr "3x3x3, 6 U"

#: kubrick.cpp:152
msgid "3x3x3, 4 U"
msgstr "3x3x3, 4 U"

#: kubrick.cpp:153
msgid "3x3x3, Snake"
msgstr "3x3x3, serp"

#: kubrick.cpp:154
msgid "3x3x3, Worm"
msgstr "3x3x3, cuc"

#: kubrick.cpp:155
msgid "3x3x3, Tricolor"
msgstr "3x3x3, tricolor"

#: kubrick.cpp:156
msgid "3x3x3, Double Cube"
msgstr "3x3x3, cub doble"

#: kubrick.cpp:162
msgid ""
"<qt>Mathematicians calculate that a 3x3x3 cube can be shuffled into "
"43,252,003,274,489,856,000 different patterns, yet they conjecture that all "
"positions can be solved in 20 moves or less.  The method that can do that "
"(as yet undiscovered) is called God's Algorithm.<br><br>Many longer methods "
"are known.  See the two Wikipedia articles on Rubik's Cube and Optimal "
"Solutions for Rubik's Cube.<br><br>Several methods work systematically by "
"building the solution one layer at a time, using sequences of moves that "
"solve a few pieces without disturbing what has already been done.  The "
"'Beginner Solution' demonstrated here uses that approach.  Just over 100 "
"moves solve a cube that is shuffled in 20.</qt>"
msgstr ""
"<qt>Els matemàtics han calculat que un cub de 3x3x3 pot ser barrejat en "
"43.252.003.274.489.856.000 patrons diferents, encara que conjecturen que "
"tots els estats poden ser resolts en 20 moviments o menys. El mètode que pot "
"aconseguir això (encara per descobrir) s'anomena l'Algoritme de Déu."
"<br><br>Es coneixen molts mètodes més llargs. Podeu consultar els dos "
"articles de la Viquipèdia (versió anglesa) sobre el cub de Rubik i la "
"solució òptima per al cub de Rubik.<br><br>Uns quants mètodes funcionen "
"construint sistemàticament la solució de capa en capa, usant seqüències de "
"moviments que solucionen unes quantes peces sense interferir en el que ja "
"està ordenat. La «solució per a principiants» demostrada aquí usa aquesta "
"aproximació. Poc més de 100 moviments per a solucionar un cub que ha estat "
"barrejat amb 20.</qt>"

#: kubrick.cpp:180
msgid "3x3x3 Layer 1, Edges First"
msgstr "3x3x3 capa 1, primer les vores"

#: kubrick.cpp:181
msgid "3x3x3 Layer 2, Edge from Bottom Right"
msgstr "3x3x3 capa 2, vora des de baix a la dreta"

#: kubrick.cpp:182
msgid "3x3x3 Layer 2, Edge from Bottom Left"
msgstr "3x3x3 capa 2, vora des de baix a l'esquerra"

#: kubrick.cpp:183
msgid "3x3x3 Layer 3, Flip Edge Pieces"
msgstr "3x3x3 capa 3, canvi de les peces de la vora"

#: kubrick.cpp:184
msgid "3x3x3 Layer 3, Place Corners"
msgstr "3x3x3 capa 3, col·locació de les cantonades"

#: kubrick.cpp:185
msgid "3x3x3 Layer 3, Twist Corners"
msgstr "3x3x3 capa 3, gir de les cantonades"

#: kubrick.cpp:186
msgid "3x3x3 Layer 3, Place Edges and DONE!"
msgstr "3x3x3 capa 3, col·locació de les vores i FET!"

#: kubrick.cpp:187
msgid "3x3x3 Cube, Complete Solution"
msgstr "Cub 3x3x3, solució completa"

#: kubrick.cpp:188
msgid "3x3x3 Swap 2 Pairs of Edges"
msgstr "3x3x3 intercanvi de 2 parells de vores"

#: kubrick.cpp:189
msgid "3x3x3 Untwist 2 Corners"
msgstr "3x3x3 gir de 2 cantonades"

#: kubrick.cpp:190
msgid "3x3x3 Flip 2 Edges"
msgstr "3x3x3 canvi de 2 vores"

#: kubrick.cpp:203
#, kde-format
msgid "&New Puzzle"
msgstr "Trencaclosques &nou"

#: kubrick.cpp:204
#, kde-format
msgid "Start a new puzzle."
msgstr "Comença un trencaclosques nou."

#: kubrick.cpp:205
#, kde-format
msgid ""
"Finish the puzzle you are working on and start a new puzzle with the same "
"dimensions and number of shuffling moves."
msgstr ""
"Acaba el trencaclosques en què treballeu i comença'n un de nou amb les "
"mateixes dimensions i nombre de moviments de barreja."

#: kubrick.cpp:213
#, kde-format
msgid "&Load Puzzle..."
msgstr "&Obre un trencaclosques..."

#: kubrick.cpp:214
#, kde-format
msgid "Reload a saved puzzle from a file."
msgstr "Torna a carregar un trencaclosques desat en un fitxer."

#: kubrick.cpp:215
#, kde-format
msgid ""
"Reload a puzzle you have previously saved on a file, including its "
"dimensions, settings, current state and history of moves."
msgstr ""
"Torna a carregar un trencaclosques que heu desat prèviament en un fitxer, "
"incloses les seves dimensions, la configuració, l'estat actual i l'historial "
"dels moviments."

#: kubrick.cpp:223
#, kde-format
msgid "&Save Puzzle..."
msgstr "&Desa el trencaclosques..."

#: kubrick.cpp:224
#, kde-format
msgid "Save the puzzle on a file."
msgstr "Desa el trencaclosques en un fitxer."

#: kubrick.cpp:225
#, kde-format
msgid ""
"Save the puzzle on a file, including its dimensions, settings, current state "
"and history of moves."
msgstr ""
"Desa el trencaclosques en un fitxer, incloses les seves dimensions, la "
"configuració, l'estat actual i l'historial dels moviments."

#: kubrick.cpp:232
#, kde-format
msgid "&Save Puzzle As..."
msgstr "&Desa el trencaclosques com a..."

#: kubrick.cpp:237
#, kde-format
msgid "Restart &Puzzle..."
msgstr "Reinicia el &trencaclosques..."

#: kubrick.cpp:238 kubrick.cpp:240
#, kde-format
msgid "Undo all previous moves and start again."
msgstr "Desfés tots els moviments previs i torna a començar."

#: kubrick.cpp:252 kubrick.cpp:253
#, kde-format
msgid "Undo the last move."
msgstr "Desfés l'últim moviment."

#: kubrick.cpp:258
#, kde-format
msgid "Redo a previously undone move."
msgstr "Refés un moviment prèviament desfet."

#: kubrick.cpp:259
#, kde-format
msgid "Redo a previously undone move (repeatedly from the start if required)."
msgstr ""
"Refés un moviment desfet prèviament (si cal, es pot fer repetidament fins al "
"començament)."

#: kubrick.cpp:265
#, kde-format
msgid "Show the solution of the puzzle."
msgstr "Mostra la solució del trencaclosques."

#: kubrick.cpp:266
#, kde-format
msgid ""
"Show the solution of the puzzle by undoing and re-doing all shuffling moves."
msgstr ""
"Mostra la solució del trencaclosques desfent i refent tots els moviments de "
"barreja."

#: kubrick.cpp:272
#, kde-format
msgid "Main &Demo"
msgstr "&Demostració principal"

#: kubrick.cpp:273
#, kde-format
msgid "Run a demonstration of puzzle moves."
msgstr "Fes una demostració dels moviments del trencaclosques."

#: kubrick.cpp:274
#, kde-format
msgid ""
"Run a demonstration of puzzle moves, in which randomly chosen cubes, bricks "
"or mats are shuffled and solved."
msgstr ""
"Fes una demostració dels moviments del trencaclosques en la que cubs, maons "
"o matrius escollits a l'atzar són barrejats i solucionats."

#: kubrick.cpp:279
#, kde-format
msgid "Realign Cube"
msgstr "Realinea el cub"

#: kubrick.cpp:280
#, kde-format
msgid ""
"Realign the cube so that the top, front and right faces are visible together."
msgstr ""
"Realinea el cub de manera que les cares superior, frontal i dreta siguin "
"visibles a la vegada."

#: kubrick.cpp:282
#, kde-format
msgid ""
"Realign the cube so that the top, front and right faces are visible together "
"and the cube's axes are parallel to the XYZ axes, thus making keyboard moves "
"properly meaningful."
msgstr ""
"Realinea el cub de manera que les cares superior, frontal i dreta siguin "
"visibles a la vegada i que els eixos del cub siguin paral·lels als eixos "
"XYZ, fent que els moviments amb el teclat tinguin un significat correcte."

#: kubrick.cpp:297 kubrick.cpp:311
#, kde-format
msgid "Singmaster Moves"
msgstr "Moviments de Singmaster"

#: kubrick.cpp:313
#, kde-format
msgid "This area shows Singmaster moves."
msgstr "Aquesta àrea mostra els moviments en notació de Singmaster."

#: kubrick.cpp:318
#, kde-format
msgctxt ""
"The letters RLFBUD are mathematical notation based on English words. Please "
"leave those letters and words untranslated in some form."
msgid ""
"This area shows Singmaster moves. They are based on the letters RLFBUD, "
"representing (in English) the Right, Left, Front, Back, Up and Down faces. "
"In normal view, the letters RFU represent clockwise moves of the three "
"visible faces and LBD appear as anticlockwise moves of the hidden faces. "
"Adding a ' (apostrophe) to a letter gives the reverse of that letter's move. "
"To move inner slices, add periods (or dots) before the letter of the nearest "
"face."
msgstr ""
"Aquesta àrea mostra els moviments en notació de Singmaster. Estan basats en "
"les lletres RLFBUD, que representen (en anglès) les cares dreta (Right), "
"esquerra (Left), frontal (Front), posterior (Back), superior (Up) i inferior "
"(Down). En la vista normal, les lletres RFU representen moviments en sentit "
"horari de les tres cares visibles i LBD apareix com a moviments antihoraris "
"de les cares ocultes. Afegint un «'» (apòstrof) a una lletra dona el "
"moviment al revers d'aquesta lletra. Per a moure els segments del mig, "
"afegiu períodes (o punts) avanç de la lletra de la cara més pròxima."

#: kubrick.cpp:342
#, kde-format
msgid "&Easy"
msgstr "&Fàcil"

#: kubrick.cpp:347
#, kde-format
msgid "&Not So Easy"
msgstr "&No tan fàcil"

#: kubrick.cpp:352
#, kde-format
msgid "&Hard"
msgstr "&Difícil"

#: kubrick.cpp:357
#, kde-format
msgid "&Very Hard"
msgstr "&Molt difícil"

#: kubrick.cpp:363
#, kde-format
msgid "Make your own..."
msgstr "Personalitza..."

#: kubrick.cpp:371
#, kde-format
msgid "1 Cube"
msgstr "1 cub"

#: kubrick.cpp:373
#, kde-format
msgid "Show one view of this cube."
msgstr "Mostra un punt de vista d'aquest cub."

#: kubrick.cpp:374
#, kde-format
msgid "Show one view of this cube, from the front."
msgstr "Mostra un sol punt de vista d'aquest cub, des del davant."

#: kubrick.cpp:381
#, kde-format
msgid "2 Cubes"
msgstr "2 cubs"

#: kubrick.cpp:383
#, kde-format
msgid "Show two views of this cube."
msgstr "Mostra dos punts de vista d'aquest cub."

#: kubrick.cpp:384
#, kde-format
msgid ""
"Show two views of this cube, from the front and the back.  Both can rotate."
msgstr ""
"Mostra dos punts de vista d'aquest cub, des del davant i des del darrere. "
"Les dues vistes poden fer rotacions."

#: kubrick.cpp:390
#, kde-format
msgid "3 Cubes"
msgstr "3 cubs"

#: kubrick.cpp:392
#, kde-format
msgid "Show three views of this cube."
msgstr "Mostra tres punts de vista d'aquest cub."

#: kubrick.cpp:393
#, kde-format
msgid ""
"Show three views of this cube, a large one, from the front, and two small "
"ones, from the front and the back.  Only the large one can rotate."
msgstr ""
"Mostra tres vistes d'aquest cub, una gran des del davant i dues petites, des "
"del davant i des del darrere. Només es pot fer rotacions des de la gran."

#: kubrick.cpp:404
#, kde-format
msgid "&Watch Shuffling"
msgstr "&Ensenya la barreja"

#: kubrick.cpp:409
#, kde-format
msgid "Watch Your &Own Moves"
msgstr "Ensenya els teus &propis moviments"

#: kubrick.cpp:424
#, kde-format
msgid "Kubri&ck Game Settings"
msgstr "Configuració del joc Kubri&ck"

#: kubrick.cpp:428
#, kde-format
msgid "Keyboard S&hortcut Settings"
msgstr "Configuració de les &dreceres de teclat"

#: kubrick.cpp:436
#, kde-format
msgid "X Axis"
msgstr "Eix X"

#: kubrick.cpp:441
#, kde-format
msgid "Y Axis"
msgstr "Eix Y"

#: kubrick.cpp:446
#, kde-format
msgid "Z Axis"
msgstr "Eix Z"

#: kubrick.cpp:454
#, kde-format
msgid "Slice %1"
msgstr "Segment %1"

#: kubrick.cpp:461
#, kde-format
msgid "Turn whole cube"
msgstr "Gira el cub sencer"

#: kubrick.cpp:467
#, kde-format
msgid "Anti-clockwise"
msgstr "Sentit antihorari"

#: kubrick.cpp:472
#, kde-format
msgid "Clockwise"
msgstr "Sentit horari"

#: kubrick.cpp:477
#, kde-format
msgid "Move 'Up' face"
msgstr "Mou la cara de dalt"

#: kubrick.cpp:479
#, kde-format
msgid "Move 'Down' face"
msgstr "Mou la cara de baix"

#: kubrick.cpp:481
#, kde-format
msgid "Move 'Left' face"
msgstr "Mou la cara esquerra"

#: kubrick.cpp:483
#, kde-format
msgid "Move 'Right' face"
msgstr "Mou la cara dreta"

#: kubrick.cpp:485
#, kde-format
msgid "Move 'Front' face"
msgstr "Mou la cara frontal"

#: kubrick.cpp:487
#, kde-format
msgid "Move 'Back' face"
msgstr "Mou la cara del darrere"

#: kubrick.cpp:489
#, kde-format
msgid "Anti-clockwise move"
msgstr "Moviment en sentit antihorari"

#: kubrick.cpp:491
#, kde-format
msgid "Singmaster two-slice move"
msgstr "Moviment de Singmaster de dos segments"

#: kubrick.cpp:493
#, kde-format
msgid "Singmaster anti-slice move"
msgstr "Moviment de Singmaster antisegments"

#: kubrick.cpp:495
#, kde-format
msgid "Move an inner slice"
msgstr "Mou un segment interior"

#: kubrick.cpp:497 kubrick.cpp:499
#, kde-format
msgid "Complete a Singmaster move"
msgstr "Completa un moviment de Singmaster"

#: kubrick.cpp:501
#, kde-format
msgid "Add space to Singmaster moves"
msgstr "Afegeix espai als moviments de Singmaster"

#: kubrick.cpp:506
#, kde-format
msgid "Switch Background"
msgstr "Commuta el fons"

#: kubrick.cpp:629
#, kde-format
msgid "Pretty Patterns"
msgstr "Patrons bonics"

#: kubrick.cpp:647
#, kde-format
msgid "Solution Moves"
msgstr "Moviments de solució"

#: kubrick.cpp:656
#, kde-format
msgid "%1x%2x%3 cube, %4 shuffling moves"
msgstr "Cub de %1x%2x%3, %4 moviments de barreja"

#: kubrick.cpp:660
#, kde-format
msgid "%1x%2x%3 brick, %4 shuffling moves"
msgstr "Bric de %1x%2x%3, %4 moviments de barreja"

#: kubrick.cpp:664
#, kde-format
msgid "%1x%2x%3 mat, %4 shuffling moves"
msgstr "Matriu de %1x%2x%3, %4 moviments de barreja"

#. i18n: ectx: Menu (puzzles)
#: kubrickui.rc:10
#, kde-format
msgid "&Choose Puzzle Type"
msgstr "Tria el tipus de tren&caclosques"

#. i18n: ectx: Menu (demos)
#: kubrickui.rc:28
#, kde-format
msgid "&Demos"
msgstr "&Demostracions"

#. i18n: ectx: Menu (puzzles)
#: kubrickui.rc:30
#, kde-format
msgid "&Pretty Patterns"
msgstr "&Patrons bonics"

#. i18n: ectx: Menu (puzzles)
#: kubrickui.rc:33
#, kde-format
msgid "&Solution Moves"
msgstr "&Moviments de solució"

#: main.cpp:23
#, kde-format
msgid "Kubrick"
msgstr "Kubrick"

#: main.cpp:25
#, kde-format
msgid "A game based on Rubik's Cube (TM)"
msgstr "Un joc basat en el Cub de Rubik (TM)"

#: main.cpp:27
#, kde-format
msgid "&copy; 2008 Ian Wadham"
msgstr "&copy; 2008 Ian Wadham"

#: main.cpp:30
#, kde-format
msgid "Ian Wadham"
msgstr "Ian Wadham"

#: main.cpp:30
#, kde-format
msgid "Author"
msgstr "Autor"
